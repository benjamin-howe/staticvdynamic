# Presentation Notes
* I'm not going to try to support my view of the optimal typing system, rather try to get you to think deeply about typing.
* I'll try to keep us from getting stuck in the weeds in debates about variance and semantics.
* I will try to start an animated debate about the ins and outs of various typing options.
* First, though, we'll have to get through some set-up.

## Types
* Metadata about bits in memory
* Describe to the machine what a thing is, how to work with it
* Example: integer, String, SpaceStation, NucleotideSequence
* For example, I have a shirt with a bunch of 1s and 0s on it.  People sometimes ask me what it means.  They often regret having done so.

## Type System
* A set of rules about how types work in a program.
* Helps prevent something written as a NucleotideSequence from being read as a SpaceStation
* Often classified as being either Static or Dynamic

## Static / Dynamic
* Static: the type system is enforced at compile time
* Dynamic: the type system is enforced at run time

## Static Typing:
* Protects you from nasty run-time errors.
* Tooling means we don't even have to wait to compile, we can know sooner!
* Static tooling is very robust.

## Dynamic Typing:
* Who cares!
* Open to run-time surprises
* Interesting that "splashdowns" actually has a type of undefined. No null reference crashes!
* We also get access to these nifty high-level constructs, like mutable arrays.

## Tooling:
* Without all that information baked in to the code, static analysis doesn't work
* To get design-time intelligence, you essentially have to run the code constantly
* The best you can get for code-complete suggestions is essentially guessing
* Little development effort has gone in to these tooling resources

## Rainbows, re-examined:
* What here is for the benefit of the language
* of the programmer
* of the program
* This is all bloat, as far as the actual problem at hand is

## And in JavaScript...
* Far less stuff to parse through.
* Easier to read, easier to understand.
* Ha! I win!

## Facepalm

## Code Quality
* Might have to admit, the static systems have me on code quality.
* Then again, maybe I don't
* Semantic validity is only a very small part of a program's overall quality.  This isn't worth it for me.

## Tooling
* This isn't so much a question of can't be done, dynamic analysis is just harder.
* Essentially, unit tests are dynamic analysis, as are integration tests, regression tests, UA tests...

## The Cost of Safety
* Not everything important is known at design-time

## Cool filter example
* The developer knows what he's doing

## Java insists on knowing what you're doing
* Ever find yourself bending over backwards to appease the language, when you know better?

## True Principle
* Every bit of extra control you cede to a language, or the government, in the name of safety, the less freedom you'll have.

## One more quick jab

## Conclusion
* No one answer, of course.  Some things really need all that strictness and lingual overhead.
* For the applications I build, however, I want every bit of power and freedom I can get at. You can get all the code quality you need from dynamic analysis and automated testing.

## Embrace Freedom and Responsibility
* You can do it!